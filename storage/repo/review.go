package repo

import pbr "gitlab.com/test-kafka/reviewservice/genproto/review"

// ReviewStorageI ...
type ReviewStorageI interface {
	CreateReview(*pbr.ReviewReq) (*pbr.ReviewResp, error)
	GetReviewById(*pbr.ID) (*pbr.ReviewResp, error)
	UpdateReview(*pbr.Review) (*pbr.Review, error)
	DeleteByPostId(*pbr.ID) error
	DeleteByCustomerId(*pbr.Ids) error
	GetByPostId(*pbr.ID) (*pbr.GetRewiewsRes, error)
}
