package postgres

import (
	"database/sql"
	"fmt"
	"log"

	pbr "gitlab.com/test-kafka/reviewservice/genproto/review"

	"github.com/jmoiron/sqlx"
)

type reviewRepo struct {
	db *sqlx.DB
}

// NewReviewRepo ...

func NewReviewRepo(db *sqlx.DB) *reviewRepo {
	return &reviewRepo{db: db}
}

func (r *reviewRepo) CreateReview(review *pbr.ReviewReq) (*pbr.ReviewResp, error) {
	reviewResp := &pbr.ReviewResp{}
	fmt.Println(review)
	err := r.db.QueryRow(`insert into rewiews 
	(post_id,customer_id,rating,description,name) 
	values ($1,$2,$3,$4,$5) returning post_id,customer_id,rating,description,name,id`,
		review.PostId, review.CustomerUuid, review.Rating, review.Description, review.Name).Scan(
		&reviewResp.PostId, &reviewResp.CustomerUuid, &reviewResp.Rating, &reviewResp.Description, &reviewResp.Name, &reviewResp.Id)
	if err != nil {
		return &pbr.ReviewResp{}, err
	}
	return reviewResp, nil
}

func (r *reviewRepo) GetReviewById(req *pbr.ID) (*pbr.ReviewResp, error) {
	tempReview := &pbr.ReviewResp{}
	err := r.db.QueryRow(`select post_id,customer_id,
	rating,description,
	name,id from rewiews 
	where id=$1 and deleted_at is null`, req.Id).Scan(
		&tempReview.PostId, &tempReview.CustomerUuid,
		&tempReview.Rating, &tempReview.Description,
		&tempReview.Name, &tempReview.Id)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		log.Fatal("Error while select review", err)
		return &pbr.ReviewResp{}, err
	}
	return tempReview, nil
}
func (r *reviewRepo) GetByPostId(req *pbr.ID) (*pbr.GetRewiewsRes, error) {
	rows, err := r.db.Query(`select post_id,
	customer_id,rating,
	description,name,id 
	from rewiews 
	where post_id=$1 and deleted_at is null`, req.Id)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return &pbr.GetRewiewsRes{}, err
	}
	defer rows.Close()
	response := &pbr.GetRewiewsRes{}

	for rows.Next() {
		temp := &pbr.Review{}
		err = rows.Scan(
			&temp.PostId,
			&temp.CustomerUuid,
			&temp.Rating,
			&temp.Description,
			&temp.Name,
			&temp.Id,
		)
		if err != nil {
			return &pbr.GetRewiewsRes{}, err
		}
		response.Rewiews = append(response.Rewiews, temp)
	}
	err = r.db.QueryRow(`SELECT ROUND(SUM(rating) / COUNT(rating)) from rewiews 
	   where post_id = $1 and deleted_at is null 
	   group by id`, req.Id).Scan(&response.Rating)

	if err == sql.ErrNoRows {
		return &pbr.GetRewiewsRes{}, nil
	}
	if err != nil {
		return &pbr.GetRewiewsRes{}, err
	}
	return response, nil
}

func (r *reviewRepo) UpdateReview(req *pbr.Review) (*pbr.Review, error) {
	if req.CustomerUuid != "" {
		rewResp := pbr.Review{}
		err := r.db.QueryRow(`
		UPDATE rewiews SET updated_at=NOW(),post_id=$1, 
		rating=$2,
		description=$3
		WHERE id=$4 and customer_id=$5 and deleted_at is null returning id,post_id,customer_id,rating,description
	`, req.PostId,
			req.Rating, req.Description, req.Id, req.CustomerUuid).Scan(
			&rewResp.Id, &rewResp.PostId,
			&rewResp.CustomerUuid, &rewResp.Rating, &rewResp.Description)
		if err == sql.ErrNoRows {
			return &pbr.Review{}, nil
		}
		if err != nil {
			return nil, err
		}
		return &rewResp, nil
	} else {
		rewResp := pbr.Review{}
		err := r.db.QueryRow(`
		UPDATE rewiews SET updated_at=NOW(),post_id=$1, 
		rating=$2,
		description=$3
		WHERE id=$4 and deleted_at is null returning id,post_id,customer_id,rating,description
	`, req.PostId,
			req.Rating, req.Description, req.Id).Scan(
			&rewResp.Id, &rewResp.PostId,
			&rewResp.CustomerUuid, &rewResp.Rating, &rewResp.Description)
		if err == sql.ErrNoRows {
			return &pbr.Review{}, nil
		}
		if err != nil {
			return nil, err
		}
		return &rewResp, nil
	}
}

func (r *reviewRepo) DeleteByPostId(req *pbr.ID) error {
	_, err := r.db.Exec(`UPDATE rewiews SET deleted_at=NOW() where post_id=$1 and deleted_at is null`, req.Id)
	if err == sql.ErrNoRows {
		return nil
	}
	return err
}

func (r *reviewRepo) DeleteByCustomerId(req *pbr.Ids) error {
	_, err := r.db.Exec(`UPDATE rewiews SET deleted_at=NOW() 
	where customer_id=$1 
	and deleted_at is null`, req.Uuid)
	if err == sql.ErrNoRows {
		return nil
	}
	return err
}
