package service

import (
	"context"

	pbr "gitlab.com/test-kafka/reviewservice/genproto/review"
	l "gitlab.com/test-kafka/reviewservice/pkg/logger"
	"gitlab.com/test-kafka/reviewservice/storage"

	"github.com/jmoiron/sqlx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// ReviewService ...
type ReviewService struct {
	storage storage.IStorage
	logger  l.Logger
}

// NewPostService ...

func NewReviewService(db *sqlx.DB, log l.Logger) *ReviewService {
	return &ReviewService{
		storage: storage.NewStoragePg(db),
		logger:  log,
	}
}

func (s *ReviewService) CreateReview(ctx context.Context, req *pbr.ReviewReq) (*pbr.ReviewResp, error) {
	review, err := s.storage.Review().CreateReview(req)
	if err != nil {
		s.logger.Error("Error while insert to review", l.Any("error insert user", err))
		return &pbr.ReviewResp{}, status.Error(codes.Internal, "something went wrong, please check review info")
	}
	return review, nil
}

func (s *ReviewService) GetReviewById(ctx context.Context, req *pbr.ID) (*pbr.ReviewResp, error) {
	res, err := s.storage.Review().GetReviewById(req)
	if err != nil {
		s.logger.Error("Error while insert", l.Any("error select users", err))
		return &pbr.ReviewResp{}, status.Error(codes.Internal, "something went wrong, please check reviews info")
	}
	return res, nil
}

func (s *ReviewService) GetByPostId(ctx context.Context, req *pbr.ID) (*pbr.GetRewiewsRes, error) {
	res, err := s.storage.Review().GetByPostId(req)
	if err != nil {
		s.logger.Error("Error while getting rewiews by post id", l.Any("delete", err))
		return nil, status.Error(codes.Internal, "Please check your info")
	}
	return res, nil
}

func (s *ReviewService) UpdateReview(ctx context.Context, req *pbr.Review) (*pbr.Review, error) {
	res, err := s.storage.Review().UpdateReview(req)
	if err != nil {
		s.logger.Error("Error while updating", l.Any("Update", err))
		return &pbr.Review{}, status.Error(codes.InvalidArgument, "Please check reviews info")
	}
	return res, nil

}

func (s *ReviewService) DeleteByPostId(ctx context.Context, req *pbr.ID) (*pbr.Empty, error) {
	err := s.storage.Review().DeleteByPostId(req)
	if err != nil {
		s.logger.Error("Error while delete post", l.Any("Delete", err))
		return &pbr.Empty{}, status.Error(codes.InvalidArgument, "wrong id for delete")
	}
	return &pbr.Empty{}, nil
}

func (s *ReviewService) DeleteByCustomerId(ctx context.Context, req *pbr.Ids) (*pbr.Empty, error) {
	err := s.storage.Review().DeleteByCustomerId(req)
	if err != nil {
		s.logger.Error("Error while delete post", l.Any("Delete", err))
		return &pbr.Empty{}, status.Error(codes.InvalidArgument, "wrong id for delete")
	}
	return &pbr.Empty{}, nil
}
